package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.GameConfig;
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor;
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock;
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Golem;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Orc;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Door;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Treasure;
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import cz.cuni.gamedev.nail123.roguelike.world.builders.wavefunctioncollapse.WFCAreaBuilder;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class WaveFunctionCollapsedJavaWorld extends DungeonWorld {
    @NotNull
    @Override
    public Area buildLevel(int floor) {
        Size3D areaSize = Size3D.create(
                GameConfig.WINDOW_WIDTH - GameConfig.SIDEBAR_WIDTH,
                GameConfig.WINDOW_HEIGHT - GameConfig.LOG_AREA_HEIGHT, 1
        );

        AreaBuilder area = new WFCAreaBuilder(areaSize, areaSize).create();


        area.addAtEmptyPosition(
                area.getPlayer(),
                Position3D.create(0, 0, 0),
                areaSize
        );
         var playerPosition = area.getPlayer().getPosition();

        // Add stairs up
        if (floor > 0) area.addEntity(new Stairs(false), area.getPlayer().getPosition());

        // Add stairs down
        Map<Position3D, Integer> floodFill = Pathfinding.INSTANCE.floodFill(
                area.getPlayer().getPosition(),
                area,
                Pathfinding.INSTANCE.getEightDirectional(),
                Pathfinding.INSTANCE.getDoorOpening()
        );

        final var postprocessor = new WfcPostprocessor(area, Pathfinding.INSTANCE.getEightDirectional(), Pathfinding.INSTANCE.getFourDirectional(), Pathfinding.INSTANCE.getDoorOpening());
        postprocessor.doPostprocess();

        Position3D[] exitPositions = {};
        exitPositions = floodFill.keySet().toArray(exitPositions);

        Position3D staircasePosition = exitPositions[ kotlin.random.Random.Default.nextInt(0, exitPositions.length)];

        area.addEntity(new Stairs(), staircasePosition);

        postprocessor.placeEntities(floor);


        area.getPlayer().setPosition(playerPosition);

        return area.build();
    }


    static abstract class DungeonGraphElement{
        Set<Position3D> tiles = new HashSet<>();

        public List<Position3D> getUnocuppiedTiles(AreaBuilder area){
            return tiles.stream()
                    .filter(tile-> area.getPlayer().getPosition() != tile && area.getEntities().stream().noneMatch(e->tile.equals(e.getPosition())))
                    .toList();
        }
    }
    static class DungeonRoom extends DungeonGraphElement{
        Set<DungeonPath> paths = new HashSet<>();

        Set<DungeonRoom> getNeighbors(){
            var ret = new HashSet<DungeonRoom>();
            for(var path : paths){
                for(var room:path.rooms){
                    if(room == this) continue;
                    ret.add(room);
                }
            }
            return ret;
        }

        static int counter = 0;
        final int _index = ++counter;

        @Override
        public String toString() {
            return String.format("Room_%d", _index);
        }

    }
    static class DungeonPath extends DungeonGraphElement{
        Set<DungeonRoom> rooms = new HashSet<>();

        static int counter = 0;
        final int _index = ++counter;

        @Override
        public String toString() {
            return String.format("Path_%d", _index);
        }

    }

    record ReachabilityComponentInfo(Set<DungeonRoom> rooms, Set<DungeonPath> paths){
        public Set<DungeonGraphElement> allNodes(){
            var ret = new HashSet<DungeonGraphElement>();
            ret.addAll(rooms);
            ret.addAll(paths);
            return ret;
        }

    }

    static class WfcPostprocessor{
        final private AreaBuilder area;
        final private Function1<Position3D, List<Position3D>> movement8, movement4;
        final private Function1<GameBlock, Boolean> blocking;

        public WfcPostprocessor(AreaBuilder area, Function1<Position3D, List<Position3D>> movement8, Function1<Position3D, List<Position3D>> movement4, Function1<GameBlock, Boolean> blocking){
            this.area = area;
            this.movement8 = movement8;
            this.movement4 = movement4;
            this.blocking = blocking;
        }

        final private HashMap<DungeonGraphElement, UnionFind<ReachabilityComponentInfo>> reachabilityComponents = new HashMap<>();
        final private HashMap<Position3D, UnionFind<DungeonGraphElement>> allPositionsToNodes = new HashMap<>();
        final private HashSet<UnionFind<DungeonGraphElement>> allNodes = new HashSet<>();

        public void doPostprocess(){
            initGraph();
            removeCycles();
            connectEverythingTogether();
        }


        boolean isReachable(Position3D p){
            var tile = area.get(p);
            return tile != null && ! blocking.invoke(tile);
        }
        //tell whether this block (which itself must be reachable) belongs to a path or a room
        boolean isPath(Position3D pos){
            assert(isReachable(pos));
            boolean left  = isReachable(pos.withRelativeX(-1));
            boolean right = isReachable(pos.withRelativeX(1));
            boolean up    = isReachable(pos.withRelativeY(-1));
            boolean down  = isReachable(pos.withRelativeY(1));
            return (left && right && !(up || down))
                    || (up && down && !(left || right));
        }
        UnionFind<ReachabilityComponentInfo> getReachabilityComponent(Position3D p){
            var nodeUnion = allPositionsToNodes.get(p);;
            if(nodeUnion == null) return null;
            var componentUnion = reachabilityComponents.get(nodeUnion.getPayload());
            if(componentUnion == null) return null;
            return componentUnion.getTag();
        }

        void initGraph(){
            reachabilityComponents.clear();
            allPositionsToNodes.clear();
            allNodes.clear();
            var visited = new HashSet<Position3D>();
            area.getBlocks().forEach((searchRoot, block)->{
                if(blocking.invoke(area.get(searchRoot))) return;
                if(! visited.add(searchRoot)) return;

                var localPositionsToComponents = new DefaultValMap<Position3D, UnionFind<DungeonGraphElement>>(p->{
                    if(blocking.invoke(area.get(p))) return null;
                    DungeonGraphElement elem = isPath(p) ? new DungeonPath() : new DungeonRoom();
                    elem.tiles.add(p);
                    return new UnionFind<>(elem);
                });
                var nodesInCurrentComponent = new HashSet<UnionFind<DungeonGraphElement>>();
                Function2<DungeonGraphElement, DungeonGraphElement, DungeonGraphElement> mergeElems = (a,b)->{
                    a.tiles.addAll(b.tiles);
                    return a;
                };
                { //flood fill
                    var pending = new ArrayDeque<Position3D>();
                    pending.add(searchRoot);
                    while(! pending.isEmpty()){
                        var pos = pending.removeLast();

                        for(var neighbor: movement8.invoke(pos)){
                            if(! isReachable(neighbor)) continue;
                            if(visited.add(neighbor)){
                                pending.add(neighbor);
                            }
                        }

                        var component = localPositionsToComponents.get(pos);
                        for(var neighbor: movement8.invoke(pos)){
                            if(! isReachable(neighbor)) continue;
                            if(isPath(pos) == isPath(neighbor)){ //if this and the neighbor are the same type of area, merge them
                                component.merge(localPositionsToComponents.get(neighbor), mergeElems);
                            }
                        }
                    }
                }

                for(var u :localPositionsToComponents.Map.values()){
                    nodesInCurrentComponent.add(u.getTag());
                }

                //iterate through paths and detect what rooms they connect
                for(var component : nodesInCurrentComponent){
                    var node = component.getPayload();
                    if(node instanceof DungeonPath path){
                        for(var tile : path.tiles){
                            for(var neighbor : movement8.invoke(tile)){
                                if(! isReachable(neighbor)) continue;
                                var neighborsComponent = localPositionsToComponents.get(neighbor).getPayload();
                                if(neighborsComponent instanceof DungeonRoom neighborRoom){
                                    path.rooms.add(neighborRoom);
                                    neighborRoom.paths.add(path);
                                }
                            }
                        }
                    }
                }

                var rooms = new HashSet<DungeonRoom>();
                var paths = new HashSet<DungeonPath>();
                for(var component: nodesInCurrentComponent) {
                    var node = component.getPayload();
                    if (node instanceof DungeonRoom room)
                        rooms.add(room);
                    else if (node instanceof DungeonPath path)
                        paths.add(path);
                }
                var thisReachabilityComponent = new UnionFind<>(new ReachabilityComponentInfo(rooms, paths));
                for(var node: nodesInCurrentComponent){
                    reachabilityComponents.put(node.getPayload(), thisReachabilityComponent);
                }
                allNodes.addAll(nodesInCurrentComponent);

                allPositionsToNodes.putAll(localPositionsToComponents.Map);
                //System.out.printf("Current components: %d, all components: %d\n", nodesInCurrentComponent.size(), allNodes.size());
            });
        }

        void removeCycles(){
            //compute graph skeleton and throw away all paths that aren't in it
            for(var componentRaw: new HashSet<>(reachabilityComponents.values())){
                var component = componentRaw.getPayload();
                var skeleton = getGraphSkeleton(component);
                for (Iterator<DungeonPath> iterator = component.paths.iterator(); iterator.hasNext(); ) {
                    var path = iterator.next();
                    if(! (path.tiles.size() <= 1 || skeleton.contains(path))){
                        for(var coord : path.tiles){
                            area.getBlocks().put(coord, new Wall());
                        }
                        iterator.remove();
                    }
                }
            }
        }


        void connectEverythingTogether(){
            while(true){
                var components = new HashSet<>(this.reachabilityComponents.values().stream().map(UnionFind::getTag).toList());
                System.out.printf("Components count: %s\n", components.size());
                if(components.size() <= 2) return;
                for(var component: components){
                    var connection = findConnection(component);
                    if(connection != null) {
                        for(var p : connection.getPayload().tiles){
                            area.getBlocks().put(p, new Floor());
                            //area.addEntity(new Treasure(), p);
                        }
                        initGraph();
                    }else{
                        System.out.println("No connection found!");
                        continue;
                    }
                    break;
                }
            }
        }

        UnionFind<DungeonPath> findConnection(UnionFind<ReachabilityComponentInfo> component){
            class PathCandidate{
                public final PathCandidate last;
                final int length;
                final Position3D position;
                public PathCandidate(Position3D p){
                    this.last = null;
                    this.length = 1;
                    this.position = p;
                }
                public PathCandidate(PathCandidate last, Position3D p){
                    this.last = last;
                    this.length = last.length + 1;
                    this.position = p;
                }
                public UnionFind<DungeonPath> makePath(){
                    var ret = new DungeonPath();
                    UnionFind<DungeonPath> retUnion = new UnionFind<>(ret);
                    for(PathCandidate c = this; c != null; c =c.last){
                        ret.tiles.add(c.position);
                        var positionNodeRaw = allPositionsToNodes.get(c.position);
                        if(positionNodeRaw == null) continue;
                        var positionNode = positionNodeRaw.getPayload();
                        if(positionNode instanceof DungeonRoom room){
                            ret.rooms.add(room);
                            room.paths.add(ret);
                        }
                        else if(positionNode instanceof DungeonPath path){
                            for(var room: path.rooms) {
                                ret.rooms.add(room);
                                room.paths.add(ret);
                            }
                        }
                    }
                    for(var tile: ret.tiles){
                        allPositionsToNodes.computeIfAbsent(tile, k -> (UnionFind<DungeonGraphElement>) (Object) retUnion);
                    }
                    return retUnion;
                }
            }
            final var visited  = new HashMap<Position3D, PathCandidate>();
            final var queue = new ArrayDeque<PathCandidate>();
            for(var node: component.getPayload().allNodes()){
                for(var pos: node.tiles){
                    var pathCandidate = new PathCandidate(pos);
                    visited.put(pos, pathCandidate);
                    queue.add(pathCandidate);
                }
            }
            System.out.printf("Init queue size: %s\n", queue.size());
            while(! queue.isEmpty()){
                var current = queue.pop();
                var foundComponentUnion = getReachabilityComponent(current.position);
                if(foundComponentUnion != null && foundComponentUnion.getTag() != component.getTag()){
                    return current.makePath();
                }
                for(var neighborPos : movement4.invoke(current.position)){
                    if(area.get(neighborPos) == null) continue;
                    var visitInfo = visited.get(neighborPos);
                    if(visitInfo != null && visitInfo.length >= current.length + 1) continue;
                    visitInfo = new PathCandidate(current, neighborPos);
                    visited.put(neighborPos, visitInfo);
                    queue.add(visitInfo);
                }
            }
            return null;
        }

        public void placeEntities(int floor){
            var rand = new Random();
            final int MAX_SUPPORTED_FLOOR = 15;
            floor = Math.min(floor, MAX_SUPPORTED_FLOOR);
            float floorRatio = (floor-1.0f) / MAX_SUPPORTED_FLOOR;
            var playerPosition = area.getPlayer().getPosition();
            //var mainComponent = getReachabilityComponent(playerPosition);
            //var startingRoom  = allPositionsToNodes.get(playerPosition);
            for(var componentUnion : new HashSet<>(reachabilityComponents.values())){
                var component = componentUnion.getPayload();
                for(var room : component.rooms){
                    var tiles = room.getUnocuppiedTiles(area);
                    float enemiesRatio  = 0.035f + easeInQuad(floorRatio)*0.25f;
                    float treasureRatio = 0.025f + easeInQuad(floorRatio)*0.2f;
                    //if(startingRoom != null room == startingRoom) enemiesRatio = 0.01f;
                    int numEnemies = (int)(tiles.size()*enemiesRatio);
                    int numTreasures = (int)(tiles.size()*treasureRatio);
                    var chosenTiles = randomChoices(tiles, rand, numEnemies + numTreasures).stream().toList();
                    for(int t = 0;t<numEnemies; ++t){
                        var tile = chosenTiles.get(t);
                        var toss = rand.nextFloat();
                        if(toss > enemiesRatio*4)
                            area.addEntity(new Rat(), tile);
                        else if(toss < 0.1f)
                            area.addEntity(new Golem(), tile);
                        else
                            area.addEntity(new Orc(floor / 4), tile);
                    }
                    for(int t = 0;t<numTreasures; ++t){
                        var tile = chosenTiles.get(numEnemies + t);
                        area.addEntity(new Treasure(floor * 0.5f), tile);
                    }

                }
            }
        }



    }


    static Set<DungeonPath> getGraphSkeleton(ReachabilityComponentInfo graph){
        if(graph.rooms.size() <= 1) return new HashSet<>(); //skip degenerate edgecase

        var pathsInSkeleton = new HashSet<DungeonPath>();
        var roomsInSkeleton = new DefaultValMap<DungeonRoom, UnionFind<Object>>(k->new UnionFind<>(null));
        for(var path : graph.paths){
            if(roomsInSkeleton.Map.size() == graph.rooms.size())break;
            if(path.rooms.size() != 2) continue;
            var rooms = path.rooms.toArray(new DungeonRoom[2]);
            var component1 = roomsInSkeleton.get(rooms[0]);
            var component2 = roomsInSkeleton.get(rooms[1]);
            if(component1.getTag() != component2.getTag()){
                pathsInSkeleton.add(path);
                component1.merge(component2, null);
            }
        }
        return pathsInSkeleton;
    }

    static int toInt(boolean b){return b?1:0;}

    static boolean isPathPoint(Position3D pos, AreaBuilder area){
        return false;
    }

    static class UnionFind<TPayload>{
        private UnionFind<TPayload> _parent;
        private TPayload _payload;
        public UnionFind(TPayload payload){
            this._parent = this;
            this._payload = payload;
        }

        public void merge(UnionFind<TPayload> other, Function2<TPayload, TPayload, TPayload> payloadMerger){
            var tag = this.getTag();
            var newRoot = other.getTag();
            if(tag == newRoot) return;
            tag._parent = newRoot;
            if(tag._payload != null){
                if(payloadMerger != null)
                    newRoot._payload = payloadMerger.invoke(newRoot._payload, tag._payload);
                tag._payload = null;
            }
        }

        public UnionFind<TPayload> getTag(){
            var ret = _parent;
            for(; ret != ret._parent; ret = ret._parent);
            _parent = ret;
            return ret;
        }
        public TPayload getPayload(){
            return getTag()._payload;
        }

        @Override
        public String toString() {
            return getPayload() != null ? getPayload().toString() : super.toString();
        }
    }

    static class DefaultValMap<TKey, TValue>{

        public final Map<TKey, TValue> Map;
        public final Function1<TKey, TValue> ValueSupplier;

        public DefaultValMap(Function1<TKey, TValue> valueSupplier){
            Map = new HashMap<>();
            ValueSupplier = valueSupplier;
        }

        public TValue get(TKey key){
            if(! Map.containsKey(key)){
                var ret = ValueSupplier.invoke(key);
                Map.put(key, ret);
                return ret;
            }
            return Map.get(key);
        }
    }

    static float easeInQuad(final float f){
        assert f >= 0.0f && f <= 1.0f;
        return f * f;
    }
    static<T> Set<T> randomChoices(List<T> elements,Random rand, int numElements){
        assert numElements <= elements.size();
        Set<T> ret = new HashSet<>(numElements);
        while(ret.size() < numElements){
            ret.add(elements.get(rand.nextInt(elements.size()))); //since this is a set, add() cannot add duplicitly
        }
        return ret;
    }

    static<T> void shuffleInPlace(List<T> elements, Random rand){
        for(int t = 0;t<elements.size()-1;t++){
            final int other = rand.nextInt(t + 1, elements.size());
            final T temp = elements.get(t);
            elements.set(t, elements.get(other));
            elements.set(other, temp);
        }
    }
}
