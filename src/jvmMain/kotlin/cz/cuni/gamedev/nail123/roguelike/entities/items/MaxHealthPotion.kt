package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class MaxHealthPotion(val healPower: Int): Item(GameTiles.HEART_FULL) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        //if(! character.inventory.items.contains(this) )
        //    //prevent autoequip on grab
        //    return Inventory.EquipResult(false, "");

        return Inventory.EquipResult.Success
    }

    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.maxHitpoints += healPower
            character.inventory.remove(this)
            character.logMessage("Increased MAX HP by $healPower")
        }
    }

    override fun onUnequip(character: HasInventory) {}

    override fun toString(): String {
        return "MAX_HP Up($healPower)"
    }
}