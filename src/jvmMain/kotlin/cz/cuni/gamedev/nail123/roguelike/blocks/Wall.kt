package cz.cuni.gamedev.nail123.roguelike.blocks

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.items.Pickaxe
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Tile

class Wall: GameBlock(GameTiles.WALL), Interactable {
    override val blocksMovement = true
    override val blocksVision = true

    override var baseTile: Tile by GameTiles.wallTiling

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType): Boolean {
        if(other != area.player) return false;
        val pickaxe : Pickaxe? = area.player.inventory.equipped.find { it is Pickaxe } as Pickaxe?;
        if(pickaxe == null) return false;
        area.setBlockAt(this.position, MinedFloor())

        logMessage("Mined block at position [${position.x}, ${position.y}]")
        pickaxe.decreaseDurability()
        return true;
    }
}