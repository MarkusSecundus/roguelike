package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.MovingEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.items.MaxHealthPotion
import cz.cuni.gamedev.nail123.roguelike.entities.items.Pickaxe
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.extensions.chebyshevDistance
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.random.Random

class Golem: Enemy(GameTiles.GOLEM), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 15

    override val maxHitpoints = 15
    override var hitpoints = 10
    override var attackBase = 2
    override var attackRandomized = 1
    override var defense = 2

    override fun update() {
        var toAttack : MovingEntity? = null;
        for(e in this.area.entities) {
            if(e!=this &&(e is Enemy || e is Player))
            {
                val dist = Pathfinding.manhattan(e.position, position);
                if(dist <= smellingRadius && (toAttack == null || dist < Pathfinding.manhattan(toAttack.position, position)))
                    toAttack = e as MovingEntity;
            }
        }
        if(toAttack != null){
            goBlindlyTowards(toAttack.position);
        }
    }

    override fun die() {
        super.die()
        val toss = Random.nextFloat()
        if(toss < 0.1f)
            this.block.entities.add(MaxHealthPotion(1))
        else
            this.block.entities.add(Sword((3 + Random.Default.nextInt(-1, 3)).toInt(), (3+ Random.Default.nextInt(-1, 3)).toInt()))
    }
}