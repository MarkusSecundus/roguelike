package cz.cuni.gamedev.nail123.roguelike.blocks

import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class MinedFloor: GameBlock(GameTiles.FLOOR_MINED)