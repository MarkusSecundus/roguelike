package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class HealthPotion(val healPower: Int): Item(GameTiles.POTION) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        if(! character.inventory.items.contains(this) )
            //prevent autoequip on grab
            return Inventory.EquipResult(false, "");

        val healable = character as HasCombatStats?
        if(healable != null && healable.hitpoints == healable.maxHitpoints){
            return Inventory.EquipResult(false, "HP already full")
        }
        return Inventory.EquipResult.Success
    }

    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            val hpDelta = healPower.coerceAtMost(character.maxHitpoints - character.hitpoints)
            character.hitpoints += hpDelta
            character.inventory.remove(this)
            character.logMessage("Healed for $hpDelta HP")
        }
    }

    override fun onUnequip(character: HasInventory) {}

    override fun toString(): String {
        return "Health Potion($healPower )"
    }
}