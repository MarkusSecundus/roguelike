package cz.cuni.gamedev.nail123.roguelike.entities.attributes

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import kotlin.random.Random
import kotlin.random.nextInt

interface HasCombatStats {
    val maxHitpoints: Int
    var hitpoints: Int
    var attackBase: Int
    var attackRandomized: Int
    var defense: Int

    fun getAttack() : Int{
        return attackBase + Random.nextInt(attackRandomized + 1)
    }

    fun takeDamage(amount: Int) {
        hitpoints -= amount
        if (hitpoints <= 0) {
            hitpoints = 0
            die()
        }
    }
    fun die() {
        (this as GameEntity?)?.area?.removeEntity(this)
    }
}