package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Pickaxe(val attackPower: Int, val attackPowerRandom : Int, var durability : Int): Weapon(GameTiles.PICKAXE) {
    var equippingCharacter : HasInventory? = null;
    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.attackBase += attackPower
            character.attackRandomized += attackPowerRandom
        }
        equippingCharacter = character;
    }

    override fun onUnequip(character: HasInventory) {
        if (character is Player) {
            character.attackBase -= attackPower
            character.attackRandomized -= attackPowerRandom
        }
        equippingCharacter = null;
    }

    override fun toString(): String {
        return "Pickaxe($attackPower - ${attackPower + attackPowerRandom} ; $durability)"
    }
    fun decreaseDurability(){
        if(--durability <= 0){
            equippingCharacter?.inventory?.remove(this);
            logMessage("Pickaxe was destroyed")
        }
    }
}