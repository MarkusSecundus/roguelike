package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.items.MaxHealthPotion
import cz.cuni.gamedev.nail123.roguelike.entities.items.Pickaxe
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.random.Random

class Orc(val level:Int): Enemy(GameTiles.ORC), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 7

    override val maxHitpoints = 10
    override var hitpoints = 12
    override var attackBase = 3
    override var attackRandomized = 2
    override var defense = 1

    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
    }

    override fun die() {
        super.die()
        val toss = Random.nextFloat()
        if(toss < 0.1f)
            this.block.entities.add(MaxHealthPotion(1))
        else
            this.block.entities.add(Sword((5 +level*0.5f+ Random.Default.nextInt(-1, 3)).toInt(), (3+level*0.65f + Random.Default.nextInt(-1, 3)).toInt()))
    }
}