package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.Game
import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.items.HealthPotion
import cz.cuni.gamedev.nail123.roguelike.entities.items.MaxHealthPotion
import cz.cuni.gamedev.nail123.roguelike.entities.items.Pickaxe
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom
import kotlin.random.Random
import kotlin.random.nextInt

class Treasure(var preciousness : Float): GameEntity(GameTiles.CHEST), Interactable {
    val isOpenProperty = createPropertyFrom(false)
    var isOpen by isOpenProperty.asDelegate()

    override val blocksMovement: Boolean
        get() = !isOpen
    override val blocksVision: Boolean
        get() = !isOpen

    private val self : Treasure
        get() = this;

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) {
            area.removeEntity(self);
            area.addEntity(getRandomDrop(), position);
        }
    }
    private fun getRandomDrop() : GameEntity{
        when (Random.nextInt(2)){
            0->{

                return Pickaxe(1, (1.9f + preciousness*1.14f).toInt(), (8.0f + preciousness*1.2f).toInt());
            }
            1->{
                val MAX_HEALTH_POTION_FLOOR = 3;
                if(preciousness > MAX_HEALTH_POTION_FLOOR && Random.nextDouble() < 0.3)
                    return MaxHealthPotion((Math.max(1.0f, preciousness + 1.0f - MAX_HEALTH_POTION_FLOOR)*2).toInt());
                return HealthPotion((2.0f + preciousness * 1.7f).toInt())
            }
            else ->throw RuntimeException("This should not happen")
        }
    }
}