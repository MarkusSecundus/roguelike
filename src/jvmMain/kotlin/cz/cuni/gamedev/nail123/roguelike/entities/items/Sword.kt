package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Sword(val attackPower: Int, val attackPowerRandom : Int): Weapon(GameTiles.SWORD) {
    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.attackBase += attackPower
            character.attackRandomized += attackPowerRandom
        }
    }

    override fun onUnequip(character: HasInventory) {
        if (character is Player) {
            character.attackBase -= attackPower
            character.attackRandomized -= attackPowerRandom
        }
    }

    override fun toString(): String {
        return "Sword($attackPower - ${attackPower + attackPowerRandom})"
    }
}